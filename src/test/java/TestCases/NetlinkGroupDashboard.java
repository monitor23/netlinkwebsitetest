package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.NetlinkGroupHomePage;

public class NetlinkGroupDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://www.netlink-group.com/");
	}

	@Test
	public void NetlinkGroupTabs() throws InterruptedException {
		NetlinkGroupHomePage nghp = new NetlinkGroupHomePage(driver);
		nghp.VerifyHomeTab();
		nghp.VerifyWhoWeAreTab();
		nghp.VerifyProjectsTab();
		nghp.VerifyWhatWeDoTab();
		nghp.VerifyMeetUsTab();		
		nghp.VerifyJoinUsButton();
		nghp.VerifyClickonLogo();
	}

}
