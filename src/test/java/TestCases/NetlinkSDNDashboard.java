package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.NetlinkSDNHomePage;

public class NetlinkSDNDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.netlinksdn.com/");
	}

	@Test
	public void NetlinkSDNTabs() throws InterruptedException {
		NetlinkSDNHomePage nshp = new NetlinkSDNHomePage(driver);
		nshp.VerifyHomepage();
		nshp.VerifyAboutTab();
		nshp.VerifyAboutUsTab();
		nshp.VerifyServiceLinks();
		nshp.VerifyExpertiseLinks();
		nshp.VerifyContactTab();
	}
}
