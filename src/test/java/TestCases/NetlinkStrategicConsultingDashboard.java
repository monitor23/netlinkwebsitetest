package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.NetlinkStrategicConsultingHomepage;

public class NetlinkStrategicConsultingDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://www.netlinksc.com/");
	}

	@Test
	public void NetlinkSCTabs() throws InterruptedException {
		NetlinkStrategicConsultingHomepage nschp = new NetlinkStrategicConsultingHomepage(driver);
		nschp.VerifyHomepage();
		nschp.VerifyFpgaRnDTab();
		nschp.VerifyPortfolioTab();
		nschp.VerifyAboutTab();
		nschp.VerifyContactTab();
		nschp.VerifyTeamTab();
	}

}
