package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.NetlinkTestlabsHomePage;

public class NetlinkTestlabsDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.netlink-testlabs.com/");
	}

	@Test
	public void NetlinkTestlabsTabs() throws InterruptedException {
		NetlinkTestlabsHomePage nthp = new NetlinkTestlabsHomePage(driver);
		nthp.VerifyHomeTab();
		nthp.VerifyAboutUsTab();
		nthp.VerifyCertificationTab();
		nthp.VerifyServicesTab();
		nthp.VerifyClientZoneTab();
		nthp.VerifyContactUsButton();
		nthp.VerifyClickonLogo();

	}

}
