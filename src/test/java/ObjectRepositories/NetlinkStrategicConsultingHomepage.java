package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class NetlinkStrategicConsultingHomepage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public NetlinkStrategicConsultingHomepage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void VerifyHomepage() throws InterruptedException {
		Thread.sleep(1000);
		String hTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#slide-1-layer-2")))
				.getText();
		System.out.println("Title : " + hTitle);
		Assert.assertEquals(hTitle, "IT Migration and Transformation Projects");
	}

	public void VerifyFpgaRnDTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-mainmenu-2 > li:nth-child(2) > a:nth-child(1)")))
				.click();
		String fpTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#slide-14-layer-2")))
				.getText();
		System.out.println("Title : " + fpTitle);

		Assert.assertEquals(fpTitle, "FPGA R&D");
	}

	public void VerifyPortfolioTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-mainmenu-2 > li:nth-child(3) > a:nth-child(1)")))
				.click();
		String pTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#slide-12-layer-2")))
				.getText();
		System.out.println("Title : " + pTitle);

		Assert.assertEquals(pTitle, "TRACK RECORD");
	}

	public void VerifyAboutTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-mainmenu-2 > li:nth-child(4) > a:nth-child(1)")))
				.click();
		String aTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#slide-5-layer-2")))
				.getText();
		System.out.println("Title : " + aTitle);
		Assert.assertEquals(aTitle, "ABOUT");
	}

	public void VerifyContactTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-mainmenu-2 > li:nth-child(5) > a:nth-child(1)")))
				.click();
		String cTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#slide-9-layer-2")))
				.getText();
		System.out.println("Title : " + cTitle);
		Assert.assertEquals(cTitle, "CONTACT US");

	}

	public void VerifyTeamTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-mainmenu-2 > li:nth-child(6) > a:nth-child(1)")))
				.click();
		String tTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#slide-13-layer-2")))
				.getText();
		System.out.println("Title : " + tTitle);
		Assert.assertEquals(tTitle, "PROJECT/PROGRAM MANAGEMENT TEAM");
		Thread.sleep(1000);
		driver.close();

	}

}
