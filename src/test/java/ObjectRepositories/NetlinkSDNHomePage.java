package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class NetlinkSDNHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public NetlinkSDNHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void VerifyHomepage() throws InterruptedException {
		Thread.sleep(1500);
		String hTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2.wow > span:nth-child(1)")))
				.getText();
		System.out.println("Title : " + hTitle);
		Assert.assertEquals(hTitle, "Applying Data Science");
	}

	public void VerifyAboutTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-962 > a:nth-child(1)")))
				.click();
		String aTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + aTitle);
		Assert.assertEquals(aTitle, "About");
	}

	public void VerifyAboutUsTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1618 > a:nth-child(1)")))
				.click();
		String auTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".col-lg-8 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + auTitle);
		Assert.assertEquals(auTitle, "About Us");
	}

	public void VerifyServiceLinks() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1207"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1208 > a:nth-child(1)")))
				.click();
		String tcTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + tcTitle);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1207"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1212 > a:nth-child(1)")))
				.click();
		String isTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + isTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1207"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1209 > a:nth-child(1)")))
				.click();
		String psTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + psTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1207"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1215 > a:nth-child(1)")))
				.click();
		String cpmTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + cpmTitle);

	}

	public void VerifyExpertiseLinks() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1300 > a:nth-child(1)")))
				.click();
		String esTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + esTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1330 > a:nth-child(1)")))
				.click();
		String vsTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + vsTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1329 > a:nth-child(1)")))
				.click();
		String lsTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + lsTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1328 > a:nth-child(1)")))
				.click();
		String msTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + msTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1327 > a:nth-child(1)")))
				.click();
		String osTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + osTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1326 > a:nth-child(1)")))
				.click();
		String acsTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + acsTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1325 > a:nth-child(1)")))
				.click();
		String nsTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + nsTitle);

		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-dropdown-1231"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-1324 > a:nth-child(1)")))
				.click();
		String osTitle2 = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + osTitle2);

	}

	public void VerifyContactTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#menu-item-965 > a:nth-child(1)")))
				.click();
		String cTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".offset-lg-2 > h1:nth-child(1)")))
				.getText();
		System.out.println("Title : " + cTitle);
		Assert.assertEquals(cTitle, "Contact");
		Thread.sleep(1000);
		driver.close();

	}

}
