package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class NetlinkTestlabsHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public NetlinkTestlabsHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void VerifyHomeTab() {
		String hTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				".vc_custom_1449655605723 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h2:nth-child(1)")))
				.getText();
		System.out.println("Title : " + hTitle);
		Assert.assertEquals(hTitle, "Welcome to Netlink Testlabs");
	}

	public void VerifyAboutUsTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#nav-menu-item-3682 > a:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(2)")))
				.click();
		String auTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.cssSelector(".mkdf-title-subtitle-holder-inner > h1:nth-child(1) > span:nth-child(1)")))
				.getText();
		System.out.println("Title : " + auTitle);
		Assert.assertEquals(auTitle, "About Us");
	}

	public void VerifyCertificationTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#nav-menu-item-3733 > a:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(2)")))
				.click();
		String cTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.cssSelector(".mkdf-title-subtitle-holder-inner > h1:nth-child(1) > span:nth-child(1)")))
				.getText();
		System.out.println("Title : " + cTitle);
		Assert.assertEquals(cTitle, "Swift Release 7 Pilot, Production and Qualification Systems");
	}

	public void VerifyServicesTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#nav-menu-item-3681 > a:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(2)")))
				.click();
		String sTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.cssSelector(".mkdf-title-subtitle-holder-inner > h1:nth-child(1) > span:nth-child(1)")))
				.getText();
		System.out.println("Title : " + sTitle);
		Assert.assertEquals(sTitle, "Professional Services");

	}

	public void VerifyClientZoneTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#nav-menu-item-3884 > a:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(2)")))
				.click();
		String czTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.cssSelector(".mkdf-title-subtitle-holder-inner > h1:nth-child(1) > span:nth-child(1)")))
				.getText();
		System.out.println("Title : " + czTitle);
		Assert.assertEquals(czTitle, "Client Zone");
	}

	public void VerifyContactUsButton() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector("#text-5 > div:nth-child(1) > a:nth-child(1) > span:nth-child(1)"))).click();
		String csTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(
						By.cssSelector(".mkdf-title-subtitle-holder-inner > h1:nth-child(1) > span:nth-child(1)")))
				.getText();
		System.out.println("Title : " + csTitle);
		Assert.assertEquals(csTitle, "Contact us");
	}

	public void VerifyClickonLogo() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				".mkdf-menu-area > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")))
				.click();
		Thread.sleep(1000);
		driver.close();
	}

}
