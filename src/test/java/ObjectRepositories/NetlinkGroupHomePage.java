package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class NetlinkGroupHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public NetlinkGroupHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void VerifyHomeTab() {
		String hTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".elementor-heading-title")))
				.getText();
		System.out.println("Title : " + hTitle);
		Assert.assertEquals(hTitle, "Your partner in IT services");
	}

	public void VerifyWhoWeAreTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-1-503374ad > li:nth-child(2) > a:nth-child(1)")))
				.click();
		String wwaTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2.elementor-heading-title")))
				.getText();
		System.out.println("Title : " + wwaTitle);
		Assert.assertEquals(wwaTitle, "IT Specialists at your service");
	}

	public void VerifyProjectsTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-1-503374ad > li:nth-child(3) > a:nth-child(1)")))
				.click();
		String pTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2.elementor-heading-title")))
				.getText();
		System.out.println("Title : " + pTitle);
		Assert.assertEquals(pTitle, "Case Studies");
	}

	public void VerifyWhatWeDoTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-1-503374ad > li:nth-child(4) > a:nth-child(1)")))
				.click();
		String wwdTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2.elementor-heading-title")))
				.getText();
		System.out.println("Title : " + wwdTitle);
		Assert.assertEquals(wwdTitle, "What we Do");

	}

	public void VerifyMeetUsTab() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#menu-1-503374ad > li:nth-child(5) > a:nth-child(1)")))
				.click();
		String muTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector(".elementor-element-b905ad5 > div:nth-child(1) > h2:nth-child(1)"))).getText();
		System.out.println("Title : " + muTitle);
		Assert.assertEquals(muTitle, "Contact Us");
	}

	public void VerifyJoinUsButton() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector(".elementor-element-42082ede > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")))
				.click();
		String juTitle = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2.elementor-heading-title")))
				.getText();
		System.out.println("Title : " + juTitle);

		Assert.assertEquals(juTitle, "Join Us");
	}

	public void VerifyClickonLogo() throws InterruptedException {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".elementor-image > a:nth-child(1)")))
				.click();
		Thread.sleep(1000);
		driver.close();
	}

}
